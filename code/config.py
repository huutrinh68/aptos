class Config:
    def __init__(self):
        # train hyperparameters setting
        self.classification = False
        self.n_classes = 1
        self.img_size = 256
        self.batch_size = 10
        self.lr = 1e-4
        self.test_size = 0.15
        self.random_state = 2019
        self.n_workers = 16
        self.n_epochs = 400
        self.best_avg_loss = 100
        self.weight_decay = 1e-5
        self.n_step = 5
        self.gamma = 0.1
        self.n_splits = 5

        # test hyperparameters setting
        self.n_tta = 1
        self.coef = [0.5, 1.5, 2.5, 3.5]

        # output setting
        self.model_name = "efficientb3"
        self.output_path = "../output/"
        self.best_model = "../output/best_model_efficientb3/"
        self.logs = "../output/logs/"
        self.train_loss = "data/train_loss"
        self.train_f1 = "data/train_f1"
        self.eval_loss = "data/eval_loss"
        self.eval_f1 = "data/eval_f1"

        # input data setting
        self.test_img = "../input/test_images/"
        self.train_img = "../input/train_images/"
        self.train_img_2015 = "../input/resized_2015_2019_blindness_detection_images/resized_train_15/"
        self.train_csv = "../input/train.csv"
        self.train_csv_2015 = "../input/resized_2015_2019_blindness_detection_images/trainLabels15.csv"
        self.sample_submission_csv = "../input/sample_submission.csv"

config = Config()