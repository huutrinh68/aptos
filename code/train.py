from tqdm import tqdm
import torch 
from utils import AverageMeter, time_to_str
from sklearn.metrics import f1_score
from sklearn import metrics
from timeit import default_timer as timer
from config import config
import numpy as np
#from apex import amp
from tensorboardX import SummaryWriter
from kappa import quadratic_weighted_kappa
import scipy as sp
from functools import partial

writer = SummaryWriter()

class OptimizedRounder(object):
    def __init__(self):
        self.coef_ = 0

    def _kappa_loss(self, coef, X, y):
        X_p = np.copy(X)
        for i, pred in enumerate(X_p):
            if pred < coef[0]:
                X_p[i] = 0
            elif pred >= coef[0] and pred < coef[1]:
                X_p[i] = 1
            elif pred >= coef[1] and pred < coef[2]:
                X_p[i] = 2
            elif pred >= coef[2] and pred < coef[3]:
                X_p[i] = 3
            else:
                X_p[i] = 4

        ll = metrics.cohen_kappa_score(y, X_p, weights='quadratic')
        return -ll

    def fit(self, X, y):
        loss_partial = partial(self._kappa_loss, X=X, y=y)
        initial_coef = [0.5, 1.5, 2.5, 3.5]
        self.coef_ = sp.optimize.minimize(loss_partial, initial_coef, method='nelder-mead')

    def predict(self, X, coef):
        X_p = np.copy(X)
        for i, pred in enumerate(X_p):
            if pred < coef[0]:
                X_p[i] = 0
            elif pred >= coef[0] and pred < coef[1]:
                X_p[i] = 1
            elif pred >= coef[1] and pred < coef[2]:
                X_p[i] = 2
            elif pred >= coef[2] and pred < coef[3]:
                X_p[i] = 3
            else:
                X_p[i] = 4
        return X_p

    def coefficients(self):
        return self.coef_['x']



def weighted_mse_loss(preds, target, weight):
    return torch.sum(weight * (preds - target) ** 2)


def train_model(model, epoch, optimizer, criterion, train_loader, valid_loss, device, best_results, log, start, classification=True):
    if epoch == 1:
        for param in model.parameters():
                param.requires_grad = True
    avg_loss = 0.
    optimizer.zero_grad()
    losses = AverageMeter()
    f1 = AverageMeter()
    model.train() 
    preds = []
    full_labels = []
    for idx, batch in enumerate(train_loader):
        if classification:
            inputs = batch["image"]
            labels = batch["label"]
            inputs = inputs.to(device, dtype=torch.float)
            labels = labels.to(device, dtype=torch.long)
        else:
            inputs = batch["image"]
            labels = batch["label"].view(-1, 1)
            inputs = inputs.to(device, dtype=torch.float)
            labels = labels.to(device, dtype=torch.float)
        outputs = model(inputs)
        preds.append(outputs.squeeze(-1).detach().cpu().numpy())
        full_labels.append(labels.squeeze(-1).cpu().numpy())
        weights = labels[:, 0].clone()
        # for i in range(len(label_weights)):
        #     weights[labels[:, 0] == i] = label_weights[i]
        loss = criterion(outputs, labels)
        # loss = criterion(outputs, labels, weights)
        loss.backward()
        losses.update(loss.item(), inputs.size(0))
        

        optimizer.step() 
        optimizer.zero_grad() 
        
        print('\r',end='',flush=True)
        message = '%s %5.1f %6.1f         |         %0.3f  %0.3f           |         %0.3f  %0.4f         |         %s  %s    | %s' % (\
                "train", idx/len(train_loader) + epoch, epoch,
                losses.avg, f1.avg,
                valid_loss[0], valid_loss[1],
                str(best_results[0])[:8],str(best_results[1])[:8],
                time_to_str((timer() - start),'min'))
        print(message , end='',flush=True)

    # optR = OptimizedRounder()
    # optR.fit(preds, full_labels)
    # coefficients = optR.coefficients()
    # valid_predictions = optR.predict(preds, coefficients)
    preds = np.concatenate(preds).ravel().tolist()
    full_labels = np.concatenate(full_labels).ravel().tolist()
    print("preds", preds)
    print("full_labels", full_labels)
    kappa_score = metrics.cohen_kappa_score(preds, full_labels, weights='quadratic')

    # f1_batch = f1_score(labels.cpu(), outputs.argmax(-1).cpu().detach().numpy(), average='macro')
    f1.update(kappa_score, 1)
    
    writer.add_scalar(config.train_loss, losses.avg, epoch)
    writer.add_scalar(config.train_f1, f1.avg, epoch)

    log.write("\n")
        
    return [losses.avg, f1.avg]


def valid_model(model, epoch, criterion, val_loader, train_loss, device, best_results, log, start, classification=True):
    avg_val_loss = 0.
    losses = AverageMeter()
    f1 = AverageMeter()
    model.eval()
    with torch.no_grad():
        for idx, batch in enumerate(val_loader):
            if classification:
                inputs = batch["image"]
                labels = batch["label"]
                inputs = inputs.to(device, dtype=torch.float)
                labels = labels.to(device, dtype=torch.long)
            else:
                inputs = batch["image"]
                labels = batch["label"].view(-1, 1)
                inputs = inputs.to(device, dtype=torch.float)
                labels = labels.to(device, dtype=torch.float)
            outputs = model(inputs)
            loss = criterion(outputs, labels)
            losses.update(loss.item(), inputs.size(0))
            f1_batch = f1_score(labels.cpu(), outputs.argmax(-1).cpu().detach().numpy(), average='macro')
            f1.update(f1_batch, inputs.size(0))
            print('\r',end='',flush=True)
            message = '%s   %5.1f %6.1f         |         %0.3f  %0.3f           |         %0.3f  %0.4f         |         %s  %s    | %s' % (\
                    "val", idx/len(val_loader) + epoch, epoch,
                    train_loss[0], train_loss[1],
                    losses.avg, f1.avg,
                    str(best_results[0])[:8], str(best_results[1])[:8],
                    time_to_str((timer() - start),'min'))
            print(message, end='',flush=True)
        log.write("\n")

    writer.add_scalar(config.eval_loss, losses.avg, epoch)
    writer.add_scalar(config.eval_f1, f1.avg, epoch)

        
    return [losses.avg, f1.avg]
