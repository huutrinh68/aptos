from sklearn.metrics import cohen_kappa_score
from kappa import quadratic_weighted_kappa 
import numpy as np
# label = [[0.0], [4.0], [0.0], [0.0], [2.0], [0.0], [0.0], [2.0], [0.0], [0.0]]
# predict = [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]

label = [[0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0]]
predict = [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]

score = cohen_kappa_score(label, predict, labels=[0, 1, 2, 3, 4], weights="quadratic")
if np.isnan(score):
    score = 1.0
print(score)

