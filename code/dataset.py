import numpy as np 
import cv2 
from PIL import Image
from torch.utils.data import Dataset
from torchvision import transforms
from os.path import isfile
import albumentations
from albumentations.pytorch import ToTensor
import pandas as pd
import os
import torch
from config import config

def train_transform():
    # return albumentations.Compose([
    # albumentations.Resize(224, 224),
    # albumentations.RandomRotate90(p=0.5),
    # albumentations.Transpose(p=0.5),
    # albumentations.Flip(p=0.5),
    # albumentations.OneOf([
    #     albumentations.CLAHE(clip_limit=2), albumentations.RandomBrightness(), albumentations.RandomContrast(),
    #     albumentations.JpegCompression(), albumentations.Blur(), albumentations.GaussNoise()], p=0.5), 
    # albumentations.HueSaturationValue(p=0.5), 
    # albumentations.ShiftScaleRotate(shift_limit=0.15, scale_limit=0.15, rotate_limit=45, p=0.5),
    # # albumentations.ShiftScaleRotate(shift_limit=0.08, scale_limit=0.08, rotate_limit=365, p=1.0),
    # # albumentations.RandomBrightnessContrast(p=1.0),
    # albumentations.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]),
    # ToTensor()])
    return albumentations.Compose([
    albumentations.ShiftScaleRotate(shift_limit=0.1, scale_limit=0.1, rotate_limit=365, p=1.0),
    albumentations.RandomBrightnessContrast(p=1.0),
    albumentations.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]),
    ToTensor()])


def test_transform():
    return albumentations.Compose([
    albumentations.Resize(config.img_size, config.img_size),
    albumentations.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]),
    ToTensor()])

transform_train = albumentations.Compose([
    albumentations.ShiftScaleRotate(
        shift_limit=0.1,
        scale_limit=0.1,
        rotate_limit=365,
        p=1.0),
    albumentations.RandomBrightnessContrast(p=1.0),
    ToTensor()
])

transform_test = albumentations.Compose([
    ToTensor()
])


def get_image_path(img_id):
    img_id = str(img_id)
    if isfile(config.train_img + img_id + ".png"):
        return config.train_img + (img_id + ".png")
    if isfile(config.train_img_2015 + img_id + '.jpg'):
       return config.train_img_2015 + (img_id + ".jpg")
    if isfile(config.test_img + img_id + ".png"):
        return config.test_img + (img_id + ".png")
    return img_id


def crop_image_from_gray(img, tol=7):
    """
    Crop out black borders
    https://www.kaggle.com/ratthachat/aptos-updated-preprocessing-ben-s-cropping
    
    NOTE: This was used to generate the pre-processed dataset
    """

    if img.ndim == 2:
        mask = img > tol
        return img[np.ix_(mask.any(1), mask.any(0))]
    elif img.ndim == 3:
        gray_img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        mask = gray_img > tol
        check_shape = img[:, :, 0][np.ix_(mask.any(1), mask.any(0))].shape[0]
        if (check_shape == 0):
            return img
        else:
            img1 = img[:, :, 0][np.ix_(mask.any(1), mask.any(0))]
            img2 = img[:, :, 1][np.ix_(mask.any(1), mask.any(0))]
            img3 = img[:, :, 2][np.ix_(mask.any(1), mask.any(0))]
            img = np.stack([img1, img2, img3], axis=-1)
        return img


def circle_crop(img):
    """
    Create circular crop around image centre

    NOTE: This was used to generate the pre-processed dataset
    """

    img = crop_image_from_gray(img)

    height, width, depth = img.shape
    largest_side = np.max((height, width))
    img = cv2.resize(img, (largest_side, largest_side))

    height, width, depth = img.shape

    x = int(width / 2)
    y = int(height / 2)
    r = np.amin((x, y))

    circle_img = np.zeros((height, width), np.uint8)
    cv2.circle(circle_img, (x, y), int(r), 1, thickness=-1)
    img = cv2.bitwise_and(img, img, mask=circle_img)
    img = crop_image_from_gray(img)

    return img


class MyDataset(Dataset):
    
    def __init__(self, dataframe, transform=None):
        self.df = dataframe
        self.transform = transform
    
    def __len__(self):
        return len(self.df)
    
    def __getitem__(self, idx):
        resize = config.img_size
        label = torch.tensor(self.df.loc[idx, 'diagnosis'])
        
        id_code = self.df.loc[idx, 'id_code']
        image = cv2.imread(get_image_path(id_code))
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        # image = crop_image_from_gray(image)
        image = circle_crop(image)
        image = cv2.resize(image, (resize, resize))
        image = cv2.addWeighted(image, 4, cv2.GaussianBlur(image, (0,0), 10), -4, 128)
        # image = transforms.ToPILImage()(image)
        
        if self.transform:
            augmented = self.transform(image=image)
            image = augmented['image']
        
        return {"image": image, "label": label}
