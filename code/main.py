import os
import sys
import time
import json
import gc
from tqdm import tqdm
from timeit import default_timer as timer
import warnings
import torch
import torch.nn.init as init
import torch.nn as nn
import pandas as pd 
import numpy as np 
from sklearn.model_selection import train_test_split, StratifiedKFold
from torch.optim import Adam, SGD, RMSprop
import torch.functional as F
import torch.nn.functional as F
#from apex import amp
from utils import seed_everything, save_checkpoint, create_folder, AverageMeter, time_to_str, create_log
from dataset import MyDataset, train_transform, test_transform, transform_train, transform_test
from train import train_model, valid_model, weighted_mse_loss
from model import my_model
from submission import create_submission
from config import config
torch.backends.cudnn.benchmark=True
warnings.filterwarnings('ignore')
# print(os.listdir("../input"))

# 1. setting device
device = "cuda" if torch.cuda.is_available() else "cpu"
# device = "cuda:1"
start = timer()


# 2. setting parameters
classification      = config.classification
tta                 = config.n_tta
num_classes         = config.n_classes
img_size            = config.img_size
batch_size          = config.batch_size
lr                  = config.lr
n_epochs            = config.n_epochs
best_avg_loss       = config.best_avg_loss
weight_decay        = config.weight_decay
n_step              = config.n_step
gamma               = config.gamma
num_workers         = config.n_workers
test_size           = config.test_size
random_state        = config.random_state
n_splits            = config.n_splits
# 2.2. input data path
test_img            = config.test_img
train_img           = config.train_img
train_img_2015      = config.train_img_2015
train_csv           = config.train_csv
train_csv_2015      = config.train_csv_2015
sample_submission   = config.sample_submission_csv
# 2.3. prepare folder to save checkpoint and setting seed
create_folder()
seed_everything()
# 2.4. logger
log = create_log()


# 3. prepare data to train, valid, test
df  = pd.read_csv(train_csv_2015)
df.rename(columns={"image":"id_code", "level":"diagnosis"}, inplace=True)
# print(df_.head())
# total_csv = pd.concat([df1, df2_], axis=0, sort=True)
total_csv = df.iloc[:500]
# free memory
del  df
gc.collect()

best_results = [np.inf, 0]
val_metrics = [np.inf, 0]
fold = 0
train_df, valid_df  = train_test_split(total_csv, test_size=test_size, random_state=random_state, stratify=total_csv.diagnosis)
train_df.reset_index(drop=True, inplace=True)
valid_df.reset_index(drop=True, inplace=True)
# 3.1. data to train
train_dataset = MyDataset(train_df, transform=train_transform())
train_data_loader = torch.utils.data.DataLoader(train_dataset, batch_size=batch_size, shuffle=True, num_workers=num_workers)
# 3.2 data to valid
valid_dataset = MyDataset(valid_df, transform=train_transform())
valid_data_loader = torch.utils.data.DataLoader(valid_dataset, batch_size=batch_size, shuffle=False, num_workers=num_workers)


# 4. create model and optimizer, criterion, scheduler setting
# 4.1 model
model = my_model()
model.to(device)
# 4.2 optimizer, criterion, scheduler setting
criterion = nn.MSELoss()
optimizer = torch.optim.Adam(model.parameters(), lr=lr, weight_decay=weight_decay)
scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=n_step, gamma=gamma)


# 5. train and validate for each epoch
for epoch in range(n_epochs):
        start_time   = time.time()
        # train
        train_metrics= train_model(model, epoch, optimizer, criterion, train_data_loader, val_metrics, device, best_results, log, start, classification)
        # validate
        val_metrics = valid_model(model, epoch, criterion, valid_data_loader, train_metrics, device, best_results, log, start, classification)
        elapsed_time = time.time() - start_time 
        # check results
        is_best_loss = val_metrics[0] < best_results[0]
        best_results[0] = min(val_metrics[0], best_results[0])
        is_best_f1 = val_metrics[1] > best_results[1]
        best_results[1] = max(val_metrics[1], best_results[1])

        # save model
        save_checkpoint({
                        "epoch":epoch + 1,
                        "model_name":config.model_name,
                        "state_dict":model.state_dict(),
                        "best_loss":best_results[0],
                        "optimizer":optimizer.state_dict(),
                        "fold":fold,
        }, is_best_loss, is_best_f1, fold, epoch)

        # print logs
        print('\r',end='', flush=True)
        log.write('%s  %5.1f %6.1f         |         %0.3f  %0.3f           |         %0.3f  %0.4f         |         %s  %s    | %s' % (\
                "best", epoch+1, epoch+1,
                train_metrics[0], train_metrics[1],
                val_metrics[0], val_metrics[1],
                str(best_results[0])[:8], str(best_results[1])[:8],
                time_to_str((timer() - start), 'min'))
                )
        log.write("\n")
        time.sleep(0.01)

        # change learning rate
        scheduler.step(epoch)


# # 6. create submission file
# # 6.1 data to test
# # test_transform  = test_transform()
# testset         = MyDataset(pd.read_csv(sample_submission), configs, transform=test_transform(configs))
# test_loader     = torch.utils.data.DataLoader(testset, batch_size=batch_size, shuffle=False)
# create_submission(model, device, configs, test_loader)
